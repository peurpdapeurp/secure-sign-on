
#include "ndn-nrf-ble-face-qsm.h"

#include "ndn-nrf-ble-face.h"
#include "../../transport/nrf-sdk-ble-adv.h"
#include "../../transport/nrf-sdk-ble-scan.h"
#include "../../transport/nrf-sdk-ble-stack.h"

#include "qsm-queue.h"

bool currently_sending = false;
queue m_qsm_queue;

int
ndn_nrf_ble_face_qsm_init() {
  qsm_queue_init(&m_qsm_queue);
}

int
ndn_nrf_ble_face_qsm_destroy() {
  qsm_queue_empty(&m_qsm_queue);
}

void
ndn_nrf_ble_adv_stopped(void) {
  printf("ndn_nrf_ble_adv_stopped got called.\n");

  if (!qsm_queue_empty(&m_qsm_queue)) {

    printf("in ndn_nrf_ble_adv_stopped, qsm_queue was not empty.\n");

    qsm_msg current_msg = qsm_queue_dequeue(&m_qsm_queue);
    const uint8_t *packet_block = current_msg.msg;
    uint32_t size = current_msg.msg_size;
    if (nrf_sdk_ble_adv_start(packet_block, size, ndn_nrf_ble_face_adv_uuid, true, NDN_NRF_BLE_ADV_NUM, 
                            ndn_nrf_ble_adv_stopped) != NRF_BLE_OP_SUCCESS) {
      printf("nrf_sdk_ble_adv_start inside of ndn_nrf_ble_adv_stopped failed.\n");
    }
  }
  else {

    currently_sending = false;

    printf("in ndn_nrf_ble_adv_stopped, qsm_queue was empty.\n");
    // this is a hack for now; since we are using ble advertising for both the ndn-lite ble face
    // and the secure sign on ble object, we will just share advertising between them; any time that
    // the ndn-lite ble face is not advertising in order to send out multicast packets, the secure
    // sign-on client will be using legacy advertising to find potential controllers
    secure_sign_on_adv_start();
  }

}

void
send_queued_msgs() {

  if (currently_sending) {
    printf("send_queued_msgs got called when we were already sending, ignoring.\n");
    return;
  }

  printf("send_queued_msgs got called, printing current qsm_queue:\n");
  qsm_queue_print_queue(&m_qsm_queue);

  qsm_msg current_msg = qsm_queue_dequeue(&m_qsm_queue);
  const uint8_t *packet_block = current_msg.msg;
  uint32_t size = current_msg.msg_size;
  if (nrf_sdk_ble_adv_start(packet_block, size, ndn_nrf_ble_face_adv_uuid, true, NDN_NRF_BLE_ADV_NUM, 
                            ndn_nrf_ble_adv_stopped) != NRF_BLE_OP_SUCCESS) {
      printf("nrf_sdk_ble_adv_start inside of ndn_nrf_ble_adv_stopped failed.\n");
  }

}

int
ndn_nrf_ble_face_qsm_send(const uint8_t* packet_block, uint32_t size) {

  printf("ndn_nrf_ble_face_qsm_send got called.\n");

  qsm_msg msg;
  memcpy(msg.msg, packet_block, size);
  msg.msg_size = size;
  qsm_queue_enqueue(&m_qsm_queue, msg);
  printf("ndn_nrf_ble_face_qsm_send got called, printing current qsm queue:\n");
  qsm_queue_print_queue(&m_qsm_queue);

  send_queued_msgs();

  return 1;
}