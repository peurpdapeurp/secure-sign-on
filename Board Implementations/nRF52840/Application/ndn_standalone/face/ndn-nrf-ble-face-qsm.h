
#ifndef NDN_NRF_BLE_FACE_QSM_H
#define NDN_NRF_BLE_FACE_QSM_H

#include <stdint.h>

// the maximum amount of messages that can be queued for transmission before the
// message holding buffer wraps around and older messages are overwritten
#define NDN_NRF_BLE_FACE_QSM_MSG_QUEUE_SIZE 10

// the qsm - "queued send module"
// whenever the ndn-nrf-ble-face sends data, it is redirected through this module, which handles:
//    - the buffering of multiple send requests
//    - sending the requested data through both ble extended advertisements, and through a unicast
//      ble connection to a sign-on controller (if there is currently one being maintained)
//            - the basic logic is simply to first send the data through extended advertising, and then
//              after the requested amount of times for that packet have been sent, to turn off extended advertising
//              and then do legacy advertising, to allow the sign-on controller to connect and also receive this packet
//            - it may be possible in the future to allow for interleaving of advertisements and connection writing,
//              but since this unicast connection is a hack anyway, and only there because the phone cannot detect extended
//              advertisements, and due to time constraints, this is the currently adopted solution

// the buffer of data to send is circular; when the buffer starts overflowing because of too many packets being sent,
// it will overwrite the oldest packet that is still pending to be sent

int
ndn_nrf_ble_face_qsm_init();

int
ndn_nrf_ble_face_qsm_destroy();

int
ndn_nrf_ble_face_qsm_send(const uint8_t* packet_block, uint32_t size);

#endif // NDN_NRF_BLE_FACE_QSM_H