
#include "qsm-queue.h"

// adapted from https://www3.cs.stonybrook.edu/~algorith/book/programs/original/queue.c

/*	queue.c

	Implementation of a FIFO queue abstract data type.

	by: Steven Skiena
	begun: March 27, 2002
*/


/*
Copyright 2003 by Steven S. Skiena; all rights reserved. 

Permission is granted for use in non-commerical applications
provided this copyright notice remains intact and unchanged.

This program appears in my book:

"Programming Challenges: The Programming Contest Training Manual"
by Steven Skiena and Miguel Revilla, Springer-Verlag, New York 2003.

See our website www.programming-challenges.com for additional information.

This book can be ordered from Amazon.com at

http://www.amazon.com/exec/obidos/ASIN/0387001638/thealgorithmrepo/

*/


#include "qsm-queue.h"

void qsm_queue_init(queue *q)
{
        q->first = 0;
        q->last = QUEUESIZE-1;
        q->count = 0;
}

void qsm_queue_enqueue(queue *q, qsm_msg x)
{
        if (q->count >= QUEUESIZE)
		printf("Warning: queue overflow enqueue x=%d\n",x);
        else {
                q->last = (q->last+1) % QUEUESIZE;
                memcpy(q->q[ q->last ].msg, x.msg, x.msg_size);
                q->q[ q->last ].msg_size = x.msg_size;
                //printf("Size of enqueued qsm_msg after shallow copying to queue: %d\n", q->q[ q->last ].msg_size);
                q->count = q->count + 1;
        }
}

qsm_msg qsm_queue_dequeue(queue *q)
{
        qsm_msg x;

        if (q->count <= 0) printf("Warning: empty queue dequeue.\n");
        else {
                x = q->q[ q->first ];
                q->first = (q->first+1) % QUEUESIZE;
                q->count = q->count - 1;
        }

        return(x);
}

int qsm_queue_empty(queue *q)
{
        if (q->count <= 0) return (TRUE);
        else return (FALSE);
}

void qsm_queue_print_queue(queue *q)
{
        int i,j;

        i=q->first;

        printf("Current number of queued elements: %d\n", q->count);
        
        while (i != q->last) {
                printf("Size of queue entry %d: %d\n", i, q->q[i].msg_size);
                i = (i+1) % QUEUESIZE;
        }
}

