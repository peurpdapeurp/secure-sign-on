
#ifndef QSM_QUEUE_H
#define QSM_QUEUE_H

// adapted from https://www3.cs.stonybrook.edu/~algorith/book/programs/original/queue.h

/*	queue.h

	Header file for queue implementation

	by: Steven Skiena
*/

/*
Copyright 2003 by Steven S. Skiena; all rights reserved. 

Permission is granted for use in non-commerical applications
provided this copyright notice remains intact and unchanged.

This program appears in my book:

"Programming Challenges: The Programming Contest Training Manual"
by Steven Skiena and Miguel Revilla, Springer-Verlag, New York 2003.

See our website www.programming-challenges.com for additional information.

This book can be ordered from Amazon.com at

http://www.amazon.com/exec/obidos/ASIN/0387001638/thealgorithmrepo/

*/

#include "ndn-nrf-ble-face-qsm.h"
#include "ndn-nrf-ble-face.h"


#define QUEUESIZE (NDN_NRF_BLE_FACE_QSM_MSG_QUEUE_SIZE)

#define TRUE 1
#define FALSE 0

typedef struct {
  uint8_t msg[NDN_NRF_BLE_MAX_PAYLOAD_SIZE];
  uint32_t msg_size;
} qsm_msg;

typedef struct {
        qsm_msg q[QUEUESIZE+1];		/* body of queue */
        int first;                      /* position of first element */
        int last;                       /* position of last element */
        int count;                      /* number of queue elements */
} queue;

void qsm_queue_init(queue *q);

void qsm_queue_enqueue(queue *q, qsm_msg x);

qsm_msg qsm_queue_dequeue(queue *q);

// returns 1 if queue is mepty, 0 if queue has members
int qsm_queue_empty(queue *q);

void qsm_queue_print_queue(queue *q);

#endif // QSM_QUEUE_H