# Library Dependencies

This project depends on the following third party libraries:

* [esp8266ndn](https://github.com/yoursunny/esp8266ndn/)
* [Streaming](http://arduiniana.org/libraries/streaming/)
* [u8g2](https://github.com/olikraus/u8g2)
* [PriUint64](https://github.com/yoursunny/PriUint64)

It's recommended to download these libraries into `$HOME/.platformio/lib` folder.
