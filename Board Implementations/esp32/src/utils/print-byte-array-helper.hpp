#ifndef PRINT_BYTE_ARRAY_HELPER_HPP
#define PRINT_BYTE_ARRAY_HELPER_HPP

#include <stdint.h>
#include <Printable.h>

class PrintByteArray : public Printable
{
public:
  explicit
  PrintByteArray(const uint8_t *arr, size_t start, size_t end);

  size_t
  printTo(Print& p) const override;

private:
  size_t
  printTo(Print & p, uint8_t X) const;

private:
  const uint8_t* m_arr;
  size_t m_start;
  size_t m_end;
};

// void
// p(uint8_t X);

// void
// printByteArray(const uint8_t *arr, int start, int end);

#endif // PRINT_BYTE_ARRAY_HELPER_HPP