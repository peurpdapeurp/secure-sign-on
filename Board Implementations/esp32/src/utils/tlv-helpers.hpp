#ifndef TLV_HELPERS_HPP
#define TLV_HELPERS_HPP

#include <stddef.h>
#include <esp8266ndn.h>
#include <Arduino.h>

bool
readTypeAndLength(ndn_TlvDecoder &decoder, uint8_t type, 
size_t &tlvTypeAndLengthSize, size_t &tlvValueSize, String debugID);


#endif // TLV_HELPERS_HPP