#ifndef NDN_TLV_TYPES_HPP
#define NDN_TLV_TYPES_HPP

constexpr uint8_t DATA_TLV_TYPE = 0x06;
constexpr uint8_t NAME_TLV_TYPE = 0x07;
constexpr uint8_t METAINFO_TLV_TYPE = 0x14;
constexpr uint8_t CONTENT_TLV_TYPE = 0x15;

#endif // NDN_TLV_TYPES_HPP