#ifndef ECDH_HELPER_HPP
#define ECDH_HELPER_HPP

#include <stdint.h>
#include <stddef.h>

#include <uECC.h>

/** \brief Wrapper object for mbedtls ecdh interface.
 */
class ECDHHelper
{
public:
  explicit ECDHHelper();

  ~ECDHHelper();

  // this is implemented using uECC
  // returns 1 on success, 0 on failure
  int
  generateEcKeyPair(uint8_t *pub, size_t *pub_length, uint8_t *pri, size_t *pri_length, uECC_Curve curve);

  // this is implemented using uECC
  // returns 1 on success, 0 on failure
  int
  deriveSharedSecretECDH(const uint8_t *pub, const uint8_t *pri, 
    uint8_t *sharedSecretBytes, uECC_Curve curve);
  
};

#endif // ECDH_HELPER_HPP