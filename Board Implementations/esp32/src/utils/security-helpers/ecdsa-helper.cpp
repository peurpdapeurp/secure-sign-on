
#include "ecdsa-helper.hpp"
#include "../logger.hpp"
#include "../print-byte-array-helper.hpp"
#include "general-security-helper.hpp"

#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>

#define LOG(...) LOGGER(ECDSAHelper, __VA_ARGS__)

ECDSAHelper::ECDSAHelper()
{
  uECC_set_rng(GeneralSecurityHelper::RNG);
}

ECDSAHelper::~ECDSAHelper()
{

}

int
ECDSAHelper::generateApplicationLevelInterestAsymmetricSignatureECC(const ndn::InterestLite& interest,  
  const uint8_t *key, size_t keyLength, uint8_t *signatureOutput, size_t *signatureLength,
  uECC_Curve curve) {

  // will just do interest signing without a signature info for now

  uint8_t encoding[1000];
  ndn::DynamicUInt8ArrayLite output(encoding, sizeof(encoding), 0);
  size_t signedPortionBeginOffset, signedPortionEndOffset, encodingLength;
  ndn_Error error;
  if ((error = ndn::Tlv0_2WireFormatLite::encodeName
      (interest.getName(), &signedPortionBeginOffset, &signedPortionEndOffset, output, &encodingLength))) {
      LOG("Error decoding interest name for signing.");
      LOG("Error: " << ndn_getErrorString(error) << endl);
      return 1;
  }

  return generateApplicationLevelAsymmetricSignatureECC(encoding, encodingLength, key, keyLength,
    signatureOutput, signatureLength, curve);

}

int
ECDSAHelper::generateApplicationLevelAsymmetricSignatureECC(uint8_t *payload, size_t payloadLength,  
  const uint8_t *key, size_t keyLength, uint8_t *signatureOutput, size_t *signatureLength,
  uECC_Curve curve) {

  uint8_t payloadDigest[ndn_SHA256_DIGEST_SIZE];
  ndn_digestSha256(payload, payloadLength, payloadDigest);

  int privateKeySize = uECC_curve_private_key_size(curve);
  int publicKeySize = uECC_curve_public_key_size(curve);

  int ret;
  *signatureLength = (privateKeySize * 2);
  if ((ret = (uECC_sign(key, payloadDigest, ndn_SHA256_DIGEST_SIZE, signatureOutput + 8, curve))) == 0)
    return 1;

  LOG("Bytes of raw generated signature by uECC:" << endl << PrintByteArray(signatureOutput+8, 0,
    publicKeySize));

  uint8_t *signatureOutputOffsetted = signatureOutput + 8;
  // uint8_t signatureOutputCopy[publicKeySize];
  // memcpy(signatureOutputCopy, signatureOutputOffsetted, publicKeySize);
  // for (int i = 0; i < publicKeySize ; i++) {
  //   signatureOutputOffsetted[i] = signatureOutputCopy[publicKeySize - 1 - i];
  // }
  // for (int i = 0; i+2 < publicKeySize; i+=2) {
  //   uint8_t temp = signatureOutputOffsetted[i];
  //   signatureOutputOffsetted[i] = signatureOutputOffsetted[i+1];
  //   signatureOutputOffsetted[i+1] = temp;
  // }
  for (int i = 0; i+4 < publicKeySize; i+=4) {
    uint8_t temp1 = signatureOutputOffsetted[i];
    uint8_t temp2 = signatureOutputOffsetted[i+1];
    uint8_t temp3 = signatureOutputOffsetted[i+2];
    uint8_t temp4 = signatureOutputOffsetted[i+3];

    signatureOutputOffsetted[i+3] = temp1;
    signatureOutputOffsetted[i+2] = temp2;
    signatureOutputOffsetted[i+1] = temp3;
    signatureOutputOffsetted[i] = temp4;
  }

  LOG("Bytes of generated signature by uECC after correcting endianness:" << endl << 
    PrintByteArray(signatureOutput+8, 0, publicKeySize));

  LOG("Bytes of signed portion hash: " << endl << PrintByteArray(payloadDigest, 0, 
    ndn_SHA256_DIGEST_SIZE) << endl);

  LOG("Bytes of raw signature generated for interest: " << endl << PrintByteArray(signatureOutput + 8, 0, *signatureLength) << endl);

  encodeSignatureBits(signatureOutput, signatureLength, curve);

  return 0;

}