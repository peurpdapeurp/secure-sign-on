#ifndef LOGGER_HPP
#define LOGGER_HPP

#include <Streaming.h>
#include <PriUint64.h>

#define LOGGER(module, ...) \
  do { \
    LOG_OUTPUT << _DEC(millis()) << " [" #module "] " << __VA_ARGS__ << "\n"; \
  } while (false)

// #define LOGGER(module, ...)

#define LOG_OUTPUT Serial

#endif // LOGGER_HPP
