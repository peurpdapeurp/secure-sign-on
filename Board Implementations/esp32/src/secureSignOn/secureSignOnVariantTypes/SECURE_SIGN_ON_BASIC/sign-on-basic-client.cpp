
#include "sign-on-basic-client.hpp"
#include "../../../utils/logger.hpp"
#include "../../../utils/print-byte-array-helper.hpp"
#include "bootstrapping-response-parser-sign-on-basic.hpp"
#include "certificate-request-response-parser-sign-on-basic.hpp"
#include "../../../utils/security-helpers/general-security-helper.hpp"

#include <PriUint64.h>

#include "../../secureSignOnTLVs/secure-sign-on-tlvs.hpp"
#include "../../../utils/tlv-helpers.hpp"
#include "../../../utils/ndn-tlv-types.hpp"

#define LOG(...) LOGGER(SignOnBasicClient, __VA_ARGS__)

SignOnBasicClient::SignOnBasicClient(ndn::Face &face, const uint8_t *deviceIdentifier, 
  const uint8_t *deviceCapabilities, const uint8_t *secureSignOnCode, size_t secureSignOnCodeLength)
    : SecureSignOnClient(face, deviceIdentifier, SIGN_ON_BASIC_CLIENT_DEVICE_IDENTIFIER_LENGTH, 
      deviceCapabilities, SIGN_ON_BASIC_CLIENT_DEVICE_CAPABILITIES_LENGTH, secureSignOnCode,
      secureSignOnCodeLength)
{
  
}

SignOnBasicClient::~SignOnBasicClient()
{
  m_face.removeHandler(this);
};

bool 
SignOnBasicClient::onboardingCompleted()
{
  return m_onboarding_completed;
}

const ndn::NameLite &
SignOnBasicClient::getNetworkPrefix()
{
  return m_networkPrefix;
}

bool 
SignOnBasicClient::begin()
{
  LOG("Checking that bootstrap prefix was parsed correctly: "
      << ndn::PrintUri(m_bootstrapPrefix) << endl);
  LOG("Checking that certificate request name component was parsed correctly: "
      << ndn::PrintUri(m_certNameComponent) << endl);

  m_face.addHandler(this);
  return true;
};

bool 
SignOnBasicClient::initiateSignOn()
{
  sendBootstrappingRequest();
};

void
SignOnBasicClient::printLastExecutionTimes()
{
  m_timer.printLastExecutionTimes();
};

bool 
SignOnBasicClient::sendBootstrappingRequest()
{
  LOG("Send bootstrapping request got called for SignOnBasicClient." << endl);

  int ret;

  m_timer.m_lastOnboardingStartTime = esp_timer_get_time();

  ndn_NameComponent nameComps[MAX_NAME_COMPS];
  ndn::InterestLite interest(nameComps, MAX_NAME_COMPS, nullptr, 0, nullptr, 0);
  interest.setName(m_bootstrapPrefix);
  interest.setMustBeFresh(true);
  interest.getName().append(m_deviceIdentifier, SIGN_ON_BASIC_CLIENT_DEVICE_IDENTIFIER_LENGTH);
  interest.getName().append(m_deviceCapabilities, SIGN_ON_BASIC_CLIENT_DEVICE_CAPABILITIES_LENGTH);

  size_t N1PubLengthTemp;
  size_t N1PriLengthTemp;

  if ((ret = generateDiffieHellmanKeyPair(m_N1Pub, &N1PubLengthTemp, m_N1Pri, &N1PriLengthTemp)) != 0) {
    LOG("Failed to generate diffie hellman key pair for bootstrapping interest.");
      return false;
  }

  m_N1PubLength = N1PubLengthTemp;
  m_N1PriLength = N1PriLengthTemp;

  // LOG("Bytes of generated N1:" << endl << PrintByteArray(m_N1Pub, 0, m_N1PubLength) << endl);
  
  // LOG("Bytes of generated N1 private key:" << endl << PrintByteArray(m_N1Pri, 0, m_N1PriLength) << endl);

  interest.getName().append(m_N1Pub, m_N1PubLength);

  uint8_t signatureBuffer[SIGN_ON_BASIC_CLIENT_APPLICATION_LEVEL_INTEREST_SIGNATURE_MAX_LENGTH];
  size_t signatureLength;
  if ((ret = generateApplicationLevelInterestAsymmetricSignature(interest, 
    signatureBuffer, &signatureLength)) != 0) {
      LOG("Failed to generate application level interest asymmetric signature.");
      LOG("Value of ret: " << ret << endl);
      return false;
  }

  LOG("Hex of ASN encoded signature: " << endl << PrintByteArray(signatureBuffer, 0, signatureLength));
  LOG("Length of ASN encoded signature: " << PriUint64<DEC>(signatureLength) << endl);

  interest.getName().append(signatureBuffer, signatureLength);

  m_face.sendInterest(interest);

  m_timer.m_lastOnboardingSentBootstrapRequestTime = esp_timer_get_time();

  LOG("<I " << ndn::PrintUri(interest.getName()) << endl << endl);

  return true;
}

bool
SignOnBasicClient::sendCertificateRequest()
{
  LOG("Send certificate request got called for SignOnBasicClient." << endl);

  ndn_NameComponent nameComps[MAX_NAME_COMPS];
  ndn::InterestLite interest(nameComps, MAX_NAME_COMPS, nullptr, 0, nullptr, 0);

  for (int i = 0; i < m_networkPrefix.size(); i++) {
    interest.getName().append(m_networkPrefix.get(i));
  }
  interest.getName().append("cert");
  interest.getName().append(m_deviceIdentifier, SIGN_ON_BASIC_CLIENT_DEVICE_IDENTIFIER_LENGTH);

  uint8_t trustAnchorDigest[ndn_SHA256_DIGEST_SIZE];
  ndn_digestSha256(m_trustAnchor, m_trustAnchorLength, trustAnchorDigest);

  LOG("Digest of trust anchor: " << endl << PrintByteArray(trustAnchorDigest, 0, 
    ndn_SHA256_DIGEST_SIZE) << endl);

  interest.getName().append(trustAnchorDigest, ndn_SHA256_DIGEST_SIZE);

  interest.getName().append(m_N1Pub, m_N1PubLength);

  uint8_t N2PubDigest[ndn_SHA256_DIGEST_SIZE];

  // LOG("Length of m_N2PubLength: " << PriUint64<DEC>(m_N2PubLength) << endl);

  ndn_digestSha256(m_N2Pub, m_N2PubLength, N2PubDigest);

  // LOG("Digest of N2pub: " << endl << PrintByteArray(N2PubDigest, 0, ndn_SHA256_DIGEST_SIZE) << endl);

  interest.getName().append(N2PubDigest, ndn_SHA256_DIGEST_SIZE);

  uint8_t signatureBuffer[300];
  size_t signatureLength;
  generateApplicationLevelInterestAsymmetricSignature(interest, signatureBuffer, &signatureLength);

  LOG("Hex of ASN encoded signature: " << endl << PrintByteArray(signatureBuffer, 0, signatureLength));
  LOG("Length of ASN encoded signature: " << PriUint64<DEC>(signatureLength) << endl);

  interest.getName().append(signatureBuffer, signatureLength);

  // interest.setCanBePrefix(true);
  interest.setMustBeFresh(true);

  m_face.sendInterest(interest);

  LOG("<I " << ndn::PrintUri(interest.getName()) << endl);

  m_timer.m_lastOnboardingSentCertificateRequestTime = esp_timer_get_time();

  return true;
}

bool
SignOnBasicClient::processData(const ndn::DataLite& data, uint64_t endpointId)
{
  LOG("Process data got called for SignOnBasicClient." << endl);
  LOG("<D " << ndn::PrintUri(data.getName()) << endl << endl);

  const ndn::NameLite& name = data.getName();

  bool (SignOnBasicClient::*f)(const ndn::DataLite&, uint64_t) = nullptr;
  if (name.get(-7).equals(m_bootstrapPrefix.get(0)) &&
      name.get(-6).equals(m_bootstrapPrefix.get(1))) {
    LOG("Received data in response to bootstrapping request." << endl);
    f = &SignOnBasicClient::processBootstrappingResponse;
  }
  else if (name.get(-7).equals(m_certNameComponent.get(0))) {
    LOG("Received data in response to certificate request." << endl);
    f = &SignOnBasicClient::processCertificateRequestResponse;
  }
  else {
    LOG("Did not recognize data response as bootstrapping response or certificate request response." << endl);
    return false;
  }

  return (this->*f)(data, endpointId);
}

bool
SignOnBasicClient::processBootstrappingResponse(const ndn::DataLite& data, uint64_t endpointId)
{
  LOG("Process bootstrapping response got called for SignOnBasicClient." << endl);

  m_timer.m_lastOnboardingReceivedBootstrapResponseTime = esp_timer_get_time();

  int ret;

  LOG("Processing bootstrapping interest response..." << endl);

  LOG("Size of data content: " << data.getContent().size() << endl);
  LOG("Bytes of data content: " << endl << PrintByteArray(data.getContent().buf(), 0, data.getContent().size()) << endl);

  LOG("Length of signature: " << data.getSignature().getSignature().size() << endl);

  const uint8_t* dataContentBytes = data.getContent().buf();

  BootstrappingResponseParserSignOnBasic 
    BootstrappingResponseParserSignOnBasic(dataContentBytes, data.getContent().size());

  size_t N2PubLengthTemp;
  size_t trustAnchorLengthTemp;

  if (!BootstrappingResponseParserSignOnBasic.processBootstrappingResponse(m_networkPrefix,
    m_trustAnchor, &trustAnchorLengthTemp, m_N2Pub, &N2PubLengthTemp)) {
    LOG("Failed to process bootstrapping response." << endl);
    return false;
  }

  m_N2PubLength = N2PubLengthTemp;
  m_trustAnchorLength = trustAnchorLengthTemp;

  LOG("Successfully processed bootstrapping response." << endl);

  LOG("Bytes of N2 pub: " << endl << PrintByteArray(m_N2Pub, 0, m_N2PubLength));

  LOG("Network prefix received: " << ndn::PrintUri(m_networkPrefix));

  size_t sharedSecretLengthTemp;

  if ((ret = deriveDiffieHellmanSharedSecret(m_sharedSecret, &sharedSecretLengthTemp)) != 0) {
    LOG("Failed to derive diffie hellman shared secret.");
    return false;
  }

  m_sharedSecretLength = sharedSecretLengthTemp;

  LOG("Bytes of generated shared secret:" << endl << 
    PrintByteArray(m_sharedSecret, 0, m_sharedSecretLength) << endl);


  if (!verifyBootstrappingResponse(data, m_secureSignOnCode)) {
    LOG("Failed to verify bootstrapping response data by Kc, ignoring bootstrapping response...");
    return false;
  }
  else {
    LOG("Successfully verified bootstrapping response by Kc.");
  }

  LOG("Bytes of stored trust anchor: " << endl << PrintByteArray(m_trustAnchor, 0, m_trustAnchorLength));

  m_lastBootstrapResponse.reset(m_face.swapPacketBuffer(nullptr));

  m_timer.m_lastOnboardingFinishedProcessingBootstrappingResponseTime = esp_timer_get_time();

  sendCertificateRequest();

  return true;

}

bool
SignOnBasicClient::processCertificateRequestResponse(const ndn::DataLite& data, uint64_t endpointId)
{

  LOG("Process certificate request response got called for SignOnBasicClient." << endl);

  m_timer.m_lastOnboardingReceivedCertificateRequestResponseTime = esp_timer_get_time();

  LOG("Attempting to verify certificate request response with this symmetric key: " << endl
    << PrintByteArray(m_sharedSecret, 0, m_sharedSecretLength) << endl);
  if (!GeneralSecurityHelper::verifyDataBySymmetricKey(data, m_sharedSecret, m_sharedSecretLength)) {
    LOG("Failed to verify certificate request response by TSK.");
    return false;
  } else {
    LOG("Successfully verified certificate request response by TSK.");
  }

  const uint8_t* dataContentBytes = data.getContent().buf();
  size_t dataContentLength = data.getContent().size();

  uint8_t encryptedKdPri[SIGN_ON_BASIC_CLIENT_KD_PRI_ENCRYPTED_MAX_LENGTH];
  size_t encryptedKdPriLength;

  uint8_t KdPubCertificateBytesTemp[SECURE_SIGN_ON_CLIENT_KD_PUB_CERTIFICATE_MAX_LENGTH];
  size_t KdPubCertificateBytesLengthTemp;
  
  LOG("Process certificate request response got called." << endl);

  CertificateRequestResponseParserSignOnBasic parser(dataContentBytes, 
    data.getContent().size());

  if (!parser.processCertificateRequestResponse
    (KdPubCertificateBytesTemp, &KdPubCertificateBytesLengthTemp, encryptedKdPri, 
    &encryptedKdPriLength)) {
    LOG("Failed to process certificate request response." << endl);
    return false;
  }

  LOG("Value of KDPubCertificateBytesLengthTemp after processing: " << 
    PriUint64<DEC>(KdPubCertificateBytesLengthTemp) << endl);

  m_KdPubCertificateBytesLength = KdPubCertificateBytesLengthTemp;
  memcpy(m_KdPubCertificateBytes, KdPubCertificateBytesTemp, m_KdPubCertificateBytesLength);

  LOG("Value of m_KDpubCertificateBytesLength after processing: " <<
    PriUint64<DEC>(m_KdPubCertificateBytesLength) << endl);

  LOG("Value of encryptedKdPriLength: " << PriUint64<DEC>(encryptedKdPriLength) << endl);
  LOG("Bytes of encrypted KdPri: " << endl << PrintByteArray(encryptedKdPri, 0, encryptedKdPriLength) << endl);

  uint8_t KdPriTemp[SIGN_ON_BASIC_CLIENT_KD_PRI_MAX_LENGTH];
  size_t KdPriLengthTemp;

  decryptKdPri(encryptedKdPri, encryptedKdPriLength, m_sharedSecret,
    SECURE_SIGN_ON_ENCRYPTION_KEY_LENGTH, KdPriTemp, &KdPriLengthTemp);

  m_KdPriLength = KdPriLengthTemp;
  memcpy(m_KdPri, KdPriTemp, m_KdPriLength);

  LOG("Bytes of received KdPub certificate: " << endl << PrintByteArray(m_KdPubCertificateBytes, 0,
    m_KdPubCertificateBytesLength) << endl);

  LOG("Value of m_KdPriLength: " << PriUint64<DEC>(m_KdPriLength) << endl);
  LOG("Bytes of received KdPri: " << endl << PrintByteArray(m_KdPri, 0, m_KdPriLength) << endl);

  m_onboarding_completed = true;

  m_timer.m_lastOnboardingFinishTime = esp_timer_get_time();

  return true;

}
