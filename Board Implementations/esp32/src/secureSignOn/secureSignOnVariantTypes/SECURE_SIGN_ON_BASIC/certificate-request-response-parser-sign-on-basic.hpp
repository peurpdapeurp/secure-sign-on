#ifndef CERTIFICATE_REQUEST_RESPONSE_PARSER_SIGN_ON_BASIC_HPP
#define CERTIFICATE_REQUEST_RESPONSE_PARSER_SIGN_ON_BASIC_HPP

#include <esp8266ndn.h>

/** \brief Parser to interpret the response to the bootstrapping request.
 */
class CertificateRequestResponseParserSignOnBasic
{
public:
  explicit
  CertificateRequestResponseParserSignOnBasic(const uint8_t* payload, size_t payloadLen);

  ~CertificateRequestResponseParserSignOnBasic();

  // 1) does quick scan to check for the expected TLV's and returns false if it doesn't find them
  // 2) populates KdPubCertificateBytes with the bytes of the received certificate from the controller
  // 3) populates KdPriEncrypted with the encrypted bytes of the private key corresponding to 
  // KdPubCertificateBytes
  bool
  processCertificateRequestResponse(uint8_t *KdPubCertificateBytes, size_t *KdPubCertificateBytesLength,
    uint8_t *KdPriEncrypted, size_t *KdPriEncryptedLength);

private:

  const uint8_t* m_payload;
  size_t m_payloadLen;

  ndn_TlvDecoder m_received_certificate_decoder;

  size_t m_receivedKdPriEncryptedTlvTypeAndLengthSize;
  size_t m_receivedKdPriEncryptedTlvValueSize;
  size_t m_receivedCertificateTlvTypeAndLengthSize;
  size_t m_receivedCertificateTlvValueSize;

  char m_keyNameComponentString[4] = "KEY";
  ndn_NameComponent m_keyNameComp[1];
  ndn::NameLite m_keyNameComponent;

};

#endif // CERTIFICATE_REQUEST_RESPONSE_PARSER_SIGN_ON_BASIC_HPP