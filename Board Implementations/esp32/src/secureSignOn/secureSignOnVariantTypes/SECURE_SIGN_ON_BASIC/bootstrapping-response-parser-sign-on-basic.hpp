#ifndef BOOTSTRAPPING_RESPONSE_PARSER_SIGN_ON_BASIC_HPP
#define BOOTSTRAPPING_RESPONSE_PARSER_SIGN_ON_BASIC_HPP

#include <esp8266ndn.h>

/** \brief Parser to interpret the response to the bootstrapping request.
 */
class BootstrappingResponseParserSignOnBasic
{
public:
  explicit
  BootstrappingResponseParserSignOnBasic(const uint8_t* payload, size_t payloadLen);

  ~BootstrappingResponseParserSignOnBasic();

  // 1) does quick scan to check for the expected TLV's and returns false if it doesn't find them
  // 2) populates the destination name with the name components of the trust anchor before
  // its KEY name component and the name component immediately preceding the KEY name component
  // 3) populates the trustanchordestination with a copy of the full trust anchor bytes
  bool
  processBootstrappingResponse(ndn::NameLite &nameDestination, uint8_t *trustAnchorDestination,
    size_t *trustAnchorLength, uint8_t *N2Pub, size_t *N2Pub_length);

public:
  const size_t&
  getN2pubTlvTypeAndLengthSize() {
    return m_N2pubTlvTypeAndLengthSize;
  };

  const size_t&
  getN2pubTlvValueSize() {
    return m_N2pubTlvValueSize;
  };

  const size_t&
  getTrustAnchorTlvTypeAndLengthSize() {
    return m_trustAnchorTlvTypeAndLengthSize;
  };

  const size_t&
  getTrustAnchorTlvValueSize() {
    return m_trustAnchorTlvValueSize;
  };
  
private:
  const uint8_t* m_payload;
  size_t m_payloadLen;

  ndn_TlvDecoder m_bootstrapping_data_decoder;
  ndn_TlvDecoder m_trust_anchor_decoder;

  size_t m_N2pubTlvTypeAndLengthSize = -1;
  size_t m_N2pubTlvValueSize = -1;
  size_t m_trustAnchorTlvTypeAndLengthSize = -1;
  size_t m_trustAnchorTlvValueSize = -1;

  char m_keyNameComponentString[4] = "KEY";
  ndn_NameComponent m_keyNameComp[1];
  ndn::NameLite m_keyNameComponent;

};

#endif // BOOTSTRAPPING_RESPONSE_PARSER_SIGN_ON_BASIC_HPP