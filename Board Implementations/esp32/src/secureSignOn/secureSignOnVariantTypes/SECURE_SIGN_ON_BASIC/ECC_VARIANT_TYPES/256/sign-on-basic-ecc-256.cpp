#include "sign-on-basic-ecc-256.hpp"

#include "../../../../../utils/logger.hpp"
#include "../../../../../utils/print-byte-array-helper.hpp"
#include "../../../../../utils/security-helpers/general-security-helper.hpp"

#include <uECC.h>

#define LOG(...) LOGGER(SignOnBasicECC256, __VA_ARGS__)

SignOnBasicECC256::SignOnBasicECC256(ndn::Face &face, const uint8_t *device_identifier,
  const uint8_t *device_capabilities, const uint8_t *secure_sign_on_code, const uint8_t *BKpub,
  const uint8_t *BKpri)
    : SignOnBasicClient(face, device_identifier, device_capabilities, secure_sign_on_code,
      SIGN_ON_BASIC_ECC_256_SECURE_SIGN_ON_CODE_LENGTH),
      m_BKpriKeyName(m_BKpriKeyNameComponents, BK_PRI_KEY_NAME_COMPONENTS_MAX_SIZE)
{
  m_BKpub = BKpub;
  m_BKpri = BKpri;
}

int 
SignOnBasicECC256::generateDiffieHellmanKeyPair(uint8_t *N1Pub, size_t *N1Pub_length,
  uint8_t *N1Pri, size_t *N1Pri_length) 
{
  return m_ecdh_helper.generateEcKeyPair(N1Pub, N1Pub_length, N1Pri, N1Pri_length, 
    SIGN_ON_BASIC_ECC_256_ECDH_AND_KD_CURVE);
}

int 
SignOnBasicECC256::generateApplicationLevelInterestAsymmetricSignature(ndn::InterestLite &interest,
  uint8_t *signatureBuffer, size_t *signatureLength)
{
  // return m_ecdsa_helper.generateApplicationLevelInterestAsymmetricSignatureECC(interest, 
  //   m_BKpub, sizeof(m_BKpub), signatureBuffer, signatureLength, SIGN_ON_BASIC_ECC_256_KS_CURVE);

  ndn::EcPrivateKey privateKey(m_BKpriKeyName);
  // it is assumed that the byte array passed in was 32 bytes in length
  // (the raw bytes of the secp256r1 private key)
  privateKey.import(m_BKpri);

  LOG("Value of BKpri: " << endl << PrintByteArray(m_BKpri, 0, 32) << endl);

  uint8_t encoding[1000];
  ndn::DynamicUInt8ArrayLite output(encoding, sizeof(encoding), 0);
  size_t signedPortionBeginOffset, signedPortionEndOffset, encodingLength;
  ndn_Error error;
  if ((error = ndn::Tlv0_2WireFormatLite::encodeName
      (interest.getName(), &signedPortionBeginOffset, &signedPortionEndOffset, output, &encodingLength))) {
      LOG("Error decoding interest name for signing.");
      LOG("Error: " << ndn_getErrorString(error) << endl);
      return 1;
  }

  uint8_t interestNameDigest[ndn_SHA256_DIGEST_SIZE];
  ndn_digestSha256(encoding, encodingLength, interestNameDigest);

  LOG("Hex string of signed portion hash: " << endl << PrintByteArray(interestNameDigest,
   0, ndn_SHA256_DIGEST_SIZE) << endl);

  if ((*signatureLength = privateKey.sign(interestNameDigest, ndn_SHA256_DIGEST_SIZE, 
      signatureBuffer)) == 0)
    return 1;

  LOG("Hex string of generated signature: " << endl << PrintByteArray(signatureBuffer,
    0, *signatureLength) << endl);

  return 0;

}

int
SignOnBasicECC256::deriveDiffieHellmanSharedSecret(uint8_t *sharedSecretBytes, 
  size_t *sharedSecretLength)
{
  *sharedSecretLength = uECC_curve_private_key_size(uECC_secp256r1());
  return m_ecdh_helper.deriveSharedSecretECDH(m_N2Pub, m_N1Pri, m_sharedSecret, 
    SIGN_ON_BASIC_ECC_256_ECDH_AND_KD_CURVE);
}

bool
SignOnBasicECC256::verifyBootstrappingResponse(const ndn::DataLite &data, const uint8_t *key)
{
  return GeneralSecurityHelper::verifyDataBySymmetricKey(data, key, 
    SIGN_ON_BASIC_ECC_256_SECURE_SIGN_ON_CODE_LENGTH);
}

void
SignOnBasicECC256::decryptKdPri(const uint8_t *encryptedKdPri, size_t encryptedKdPriLength, uint8_t *Kt, 
    const size_t KtLength, uint8_t *outputBuffer, size_t *outputLength) 
{
  //*outputLength = uECC_curve_private_key_size(SIGN_ON_BASIC_ECC_256_ECDH_AND_KD_CURVE);
  *outputLength = 32;
  GeneralSecurityHelper::decrypt(encryptedKdPri, encryptedKdPriLength, Kt, KtLength, outputBuffer);
}