#ifndef SIGN_ON_BASIC_ECC_256_CONSTS_HPP
#define SIGN_ON_BASIC_ECC_256_CONSTS_HPP

#include <uECC.h>

constexpr int SIGN_ON_BASIC_ECC_256_SECURE_SIGN_ON_CODE_LENGTH = 16;

// curve used for the diffie hellman exchange, and for the keypair generated on the
// controller side which we will get a certificate and the private key of;
// the current ecc sign on basic variants all use the same curve for both ecdh and the
// generation of the Kd keypair
const uECC_Curve SIGN_ON_BASIC_ECC_256_ECDH_AND_KD_CURVE = uECC_secp256r1();
// curve used for the bootstrapping keypair
const uECC_Curve SIGN_ON_BASIC_ECC_256_KS_CURVE = uECC_secp256r1();
// maximum number of name components in the BK pri key
const int BK_PRI_KEY_NAME_COMPONENTS_MAX_SIZE = 1;

#endif // SIGN_ON_BASIC_ECC_256_CONSTS_HPP