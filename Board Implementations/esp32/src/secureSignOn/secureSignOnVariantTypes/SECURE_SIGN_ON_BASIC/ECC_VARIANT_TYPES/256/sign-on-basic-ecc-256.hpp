#ifndef SIGN_ON_BASIC_ECC_256_HPP
#define SIGN_ON_BASIC_ECC_256_HPP

#include <PriUint64.h>

#include "../../sign-on-basic-client.hpp"
#include "sign-on-basic-ecc-256-consts.hpp"

#include "../../../../../utils/security-helpers/ecdh-helper.hpp"
#include "../../../../../utils/security-helpers/ecdsa-helper.hpp"

/** \brief Onboarding client for secure sign-on protocol.
 */
class SignOnBasicECC256 : public SignOnBasicClient
{
public:
  SignOnBasicECC256(ndn::Face& face, const uint8_t *device_identifier, const uint8_t *device_capabilities,
    const uint8_t *secure_sign_on_code, const uint8_t *BKpub, const uint8_t *BKpri);

protected:

  int
  generateDiffieHellmanKeyPair(uint8_t *N1Pub, size_t *N1Pub_length, 
    uint8_t* N1Pri, size_t *N1Pri_length);

  int
  generateApplicationLevelInterestAsymmetricSignature(ndn::InterestLite &interest,
    uint8_t *signatureBuffer, size_t *signatureLength);

  int
  deriveDiffieHellmanSharedSecret(uint8_t *sharedSecretBytes, size_t *sharedSecretLength);

  bool
  verifyBootstrappingResponse(const ndn::DataLite &data, const uint8_t *key);

  void
  decryptKdPri(const uint8_t *encryptedKdPri, size_t encryptedKdPriLength, uint8_t *Kt, 
    const size_t KtLength, uint8_t *outputBuffer, size_t *outputLength);

private:

  const uint8_t *m_BKpub;
  const uint8_t *m_BKpri;

  ndn_NameComponent m_BKpriKeyNameComponents[BK_PRI_KEY_NAME_COMPONENTS_MAX_SIZE];
  ndn::NameLite m_BKpriKeyName;

  ECDHHelper m_ecdh_helper;
  ECDSAHelper m_ecdsa_helper;

};

#endif // SIGN_ON_BASIC_ECC_256_HPP
