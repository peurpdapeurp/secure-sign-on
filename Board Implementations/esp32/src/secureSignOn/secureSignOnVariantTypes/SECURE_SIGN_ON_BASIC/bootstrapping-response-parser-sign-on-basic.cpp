
#include "bootstrapping-response-parser-sign-on-basic.hpp"
#include <ndn-cpp/c/encoding/tlv/tlv-decoder.h>
#include <ndn-cpp/c/errors.h>
#include <Streaming.h>
#include <PriUint64.h>

#include "../../../utils/print-byte-array-helper.hpp"
#include "../../../utils/logger.hpp"
#include "../../../utils/tlv-helpers.hpp"
#include "../../secureSignOnTLVs/secure-sign-on-tlvs.hpp"
#include  "../../secure-sign-on-client-consts.hpp"
#include "../../../utils/ndn-tlv-types.hpp"

#define LOG(...) LOGGER(BootstrappingResponseParserSignOnBasic, __VA_ARGS__)

BootstrappingResponseParserSignOnBasic::BootstrappingResponseParserSignOnBasic(const uint8_t* payload, size_t payloadLen)
  : m_keyNameComponent(m_keyNameComp, 1)
{
  m_payload = payload;
  m_payloadLen = payloadLen;

  ndn::parseNameFromUri(m_keyNameComponent, m_keyNameComponentString);
}

BootstrappingResponseParserSignOnBasic::~BootstrappingResponseParserSignOnBasic()
{

}

bool
BootstrappingResponseParserSignOnBasic::processBootstrappingResponse(ndn::NameLite &nameDestination, 
    uint8_t *trustAnchorDestination, size_t *trustAnchorLength, uint8_t *N2Pub, size_t *N2Pub_length)
{
  ndn_TlvDecoder_initialize(&m_bootstrapping_data_decoder, m_payload, m_payloadLen);

  if (!(readTypeAndLength(m_bootstrapping_data_decoder, N2_PUB_TLV_TYPE, 
          m_N2pubTlvTypeAndLengthSize, m_N2pubTlvValueSize, "N2 pub TLV") &&
          readTypeAndLength(m_bootstrapping_data_decoder, DATA_TLV_TYPE, 
          m_trustAnchorTlvTypeAndLengthSize, m_trustAnchorTlvValueSize, "trust anchor TLV"))) {
    return false;
  }

  ndn_TlvDecoder_initialize(&m_bootstrapping_data_decoder, m_payload, m_payloadLen);

  if (!(readTypeAndLength(m_bootstrapping_data_decoder, N2_PUB_TLV_TYPE, 
          m_N2pubTlvTypeAndLengthSize, m_N2pubTlvValueSize, "N2 pub TLV"))) {
    return false;
  }

  ndn_TlvDecoder_initialize(&m_trust_anchor_decoder, m_payload + m_bootstrapping_data_decoder.offset + m_trustAnchorTlvTypeAndLengthSize,
    m_bootstrapping_data_decoder.inputLength - m_bootstrapping_data_decoder.offset - m_trustAnchorTlvTypeAndLengthSize);

  //LOG("Bytes of trust anchor (without header): " << endl << 
  //  PrintByteArray(m_trust_anchor_decoder.input, 0, m_trust_anchor_decoder.inputLength));

  size_t trustAnchorNameTlvValueSize, trustAnchorNameTlvTypeAndLengthSize;

  if (!(readTypeAndLength(m_trust_anchor_decoder, NAME_TLV_TYPE, 
          trustAnchorNameTlvTypeAndLengthSize, trustAnchorNameTlvValueSize, "Trust anchor Name TLV"))) {
    return false;
  }

  ndn_NameComponent trustAnchorNameComps[MAX_NAME_COMPS];
  ndn::NameLite trustAnchorName(trustAnchorNameComps, MAX_NAME_COMPS);

  ndn_Error nameDecodingError;
  size_t nameSignedPortionBeginOffset, nameSignedPortionEndOffset, inputLength;
  if (nameDecodingError = ndn::Tlv0_2WireFormatLite::decodeName(trustAnchorName, 
      m_trust_anchor_decoder.input, 
      trustAnchorNameTlvTypeAndLengthSize + trustAnchorNameTlvValueSize,
      &nameSignedPortionBeginOffset, &nameSignedPortionEndOffset)) {
    LOG("Error decoding name of trust anchor.");
    LOG("Error: ");
    LOG(ndn_getErrorString(nameDecodingError));
    return false;
  }

  int keyNameComponentIndex = 0;
    for (keyNameComponentIndex = 0; keyNameComponentIndex < trustAnchorName.size(); keyNameComponentIndex++) {
      if (trustAnchorName.get(keyNameComponentIndex).equals(m_keyNameComponent.get(0))) {
        LOG("Found the KEY name component in trust anchor name at index " << keyNameComponentIndex);
        break;
    }
  }

  nameDestination.clear();
  for (int i = 0; i < keyNameComponentIndex; i++) {
      nameDestination.append(trustAnchorName.get(i));
  }

  LOG("Value of m_N2pubTlvValueSize: " << PriUint64<DEC>(m_N2pubTlvValueSize) << endl);
  *N2Pub_length = m_N2pubTlvValueSize;
  LOG("Value of N2Pub_length after setting it in bootstrap response parser: " << 
    PriUint64<DEC>(*N2Pub_length) << endl);
  *trustAnchorLength = m_trustAnchorTlvTypeAndLengthSize + m_trustAnchorTlvValueSize;

  memcpy(trustAnchorDestination, m_payload + m_N2pubTlvTypeAndLengthSize +
    m_N2pubTlvValueSize, *trustAnchorLength);
  memcpy(N2Pub, m_payload + m_N2pubTlvTypeAndLengthSize,
    m_N2pubTlvValueSize);

  return true;

}