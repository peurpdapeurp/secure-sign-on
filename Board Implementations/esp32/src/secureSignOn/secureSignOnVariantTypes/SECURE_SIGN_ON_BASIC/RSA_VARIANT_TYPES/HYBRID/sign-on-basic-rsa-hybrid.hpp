#ifndef SIGN_ON_BASIC_RSA_HYBRID_HPP
#define SIGN_ON_BASIC_RSA_HYBRID_HPP

#include <PriUint64.h>

#include "../../sign-on-basic-client.hpp"
#include "sign-on-basic-rsa-hybrid-consts.hpp"

#include "../../../../../utils/security-helpers/dh-helper.hpp"
#include "../../../../../utils/security-helpers/rsa-helper.hpp"

/** \brief Onboarding client for secure sign-on protocol.
 */
class SignOnBasicRSAHybrid : public SignOnBasicClient
{
public:
  SignOnBasicRSAHybrid(ndn::Face& face, const uint8_t *device_identifier, const uint8_t *device_capabilities,
    const uint8_t *secure_sign_on_code, const unsigned char *dh_params, size_t dh_params_length, 
    const unsigned char *rsa_pem, size_t rsa_pem_length);

protected:

  int
  generateDiffieHellmanKeyPair(uint8_t *N1Pub, size_t *N1Pub_length, 
    uint8_t* N1Pri, size_t *N1Pri_length);

  int
  generateApplicationLevelInterestAsymmetricSignature(ndn::InterestLite &interest,
    uint8_t *signatureBuffer, size_t *signatureLength);

  int
  deriveDiffieHellmanSharedSecret(uint8_t *sharedSecretBytes, size_t *sharedSecretLength);

  bool
  verifyBootstrappingResponse(const ndn::DataLite &data, const uint8_t *key);

  void
  decryptKdPri(const uint8_t *encryptedKdPri, size_t encryptedKdPriLength, uint8_t *Kt, 
    const size_t KtLength, uint8_t *outputBuffer, size_t *outputLength);

private:

  DHHelper m_dh_helper;
  RSAHelper m_rsa_helper;

};

#endif // SIGN_ON_BASIC_RSA_HYBRID_HPP
