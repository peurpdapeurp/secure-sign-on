#include "sign-on-basic-rsa-1024.hpp"

#include "../../../../../utils/logger.hpp"
#include "../../../../../utils/print-byte-array-helper.hpp"
#include "../../../../../utils/security-helpers/general-security-helper.hpp"
#include "../sign-on-basic-rsa-consts.hpp"

#define LOG(...) LOGGER(SignOnBasicRSA1024, __VA_ARGS__)

SignOnBasicRSA1024::SignOnBasicRSA1024(ndn::Face &face, const uint8_t *device_identifier,
  const uint8_t *device_capabilities, const uint8_t *secure_sign_on_code, const unsigned char *dh_params, 
    size_t dh_params_length, const unsigned char *rsa_pem, size_t rsa_pem_length)
    : SignOnBasicClient(face, device_identifier, device_capabilities, secure_sign_on_code,
      SIGN_ON_BASIC_RSA_1024_SECURE_SIGN_ON_CODE_LENGTH),
      m_dh_helper(dh_params, dh_params_length),
      m_rsa_helper(rsa_pem, rsa_pem_length)
{

}

int 
SignOnBasicRSA1024::generateDiffieHellmanKeyPair(uint8_t *N1Pub, size_t *N1Pub_length,
  uint8_t *N1Pri, size_t *N1Pri_length) 
{
  *N1Pri_length = DH_PRIVATE_EXPONENT_SIZE_BYTES;
  return m_dh_helper.generateDHKeyPair(m_N1Pub, N1Pub_length, DH_PRIVATE_EXPONENT_SIZE_BYTES);
}

int 
SignOnBasicRSA1024::generateApplicationLevelInterestAsymmetricSignature(ndn::InterestLite &interest,
  uint8_t *signatureBuffer, size_t *signatureLength)
{
  return m_rsa_helper.generateApplicationLevelInterestAsymmetricSignatureRSA(interest,
    signatureBuffer, signatureLength);
}

int
SignOnBasicRSA1024::deriveDiffieHellmanSharedSecret(uint8_t *sharedSecretBytes, 
  size_t *sharedSecretLength)
{
  LOG("m_N2PubLength from rsa1024 deriveDiffieHellmanSharedSecret: " << PriUint64<DEC>(m_N2PubLength) << endl);
  return m_dh_helper.readDHPublicAndDeriveSharedSecret(m_N2Pub, m_N2PubLength,
    sharedSecretBytes, SIGN_ON_BASIC_CLIENT_SHARED_SECRET_MAX_LENGTH, sharedSecretLength);
}

bool
SignOnBasicRSA1024::verifyBootstrappingResponse(const ndn::DataLite &data, const uint8_t *key)
{
  return GeneralSecurityHelper::verifyDataBySymmetricKey(data, key, 
    SIGN_ON_BASIC_RSA_1024_SECURE_SIGN_ON_CODE_LENGTH);
}

void
SignOnBasicRSA1024::decryptKdPri(const uint8_t *encryptedKdPri, size_t encryptedKdPriLength, uint8_t *Kt, 
    const size_t KtLength, uint8_t *outputBuffer, size_t *outputLength)
{
  *outputLength = 128;
  GeneralSecurityHelper::decrypt(encryptedKdPri, encryptedKdPriLength, Kt,
    KtLength, outputBuffer);
}