#ifndef SIGN_ON_BASIC_CLIENT_HPP
#define SIGN_ON_BASIC_CLIENT_HPP

#include <esp8266ndn.h>

#include "sign-on-basic-client-timer.hpp"
#include "sign-on-basic-client-consts.hpp"
#include "../../secure-sign-on-client.hpp"

#include <uECC.h>

/** \brief Generic client for secure sign-on protocol.
 */
class SignOnBasicClient : public SecureSignOnClient
{
public:

  SignOnBasicClient(ndn::Face &face, const uint8_t *deviceIdentifier, const uint8_t *deviceCapabilities,
    const uint8_t *secureSignOnCode, size_t secureSignOnCodeLength);

  ~SignOnBasicClient() noexcept;

  bool
  onboardingCompleted();

  const ndn::NameLite&
  getNetworkPrefix();

  bool
  begin();

  bool
  initiateSignOn();

  void
  printLastExecutionTimes();

protected:

  // returns 1 on success, 0 on failure
  virtual int
  generateDiffieHellmanKeyPair(uint8_t *N1Pub, size_t *N1Pub_length, 
    uint8_t* N1Pri, size_t *N1Pri_length) = 0;

  // returns 1 on success, 0 on failure
  virtual int
  generateApplicationLevelInterestAsymmetricSignature(ndn::InterestLite &interest,
    uint8_t *signatureBuffer, size_t *signatureLength) = 0;

  // returns 1 on success, 0 on failure
  virtual int
  deriveDiffieHellmanSharedSecret(uint8_t *sharedSecretBytes, size_t *sharedSecretLength) = 0;

  // this is pure virtual because it depends on the length of the sign on code, which differs depending
  // on which sign on basic variant is being used (e.g., ECC_160's sign on code is 10 bytes, while ECC_256's
  // sign on code is 16 bytes)
  virtual bool
  verifyBootstrappingResponse(const ndn::DataLite &data, const uint8_t *key) = 0;

  virtual void
  decryptKdPri(const uint8_t *encryptedKdPri, size_t encryptedKdPriLength, uint8_t *Kt, 
    const size_t KtLength, uint8_t *outputBuffer, size_t *outputLength) = 0;


protected:

  virtual bool
  sendBootstrappingRequest();

  virtual bool 
  sendCertificateRequest();

  virtual bool
  processBootstrappingResponse(const ndn::DataLite& data, uint64_t endpointId);

  virtual bool
  processCertificateRequestResponse(const ndn::DataLite& data, uint64_t endpointId);

  bool 
  processData(const ndn::DataLite& data, uint64_t endpointId) override;


protected:

  uint8_t m_N1Pub[SIGN_ON_BASIC_CLIENT_N1_PUB_MAX_LENGTH];
  size_t m_N1PubLength;

  uint8_t m_N1Pri[SIGN_ON_BASIC_CLIENT_N1_PRI_MAX_LENGTH];
  size_t m_N1PriLength;

  uint8_t m_N2Pub[SIGN_ON_BASIC_CLIENT_N2_PUB_MAX_LENGTH];
  size_t m_N2PubLength;

  uint8_t m_KdPub[SIGN_ON_BASIC_CLIENT_KD_PUB_MAX_LENGTH];
  size_t m_KdPubLength;
  uint8_t m_KdPri[SIGN_ON_BASIC_CLIENT_KD_PRI_MAX_LENGTH];
  size_t m_KdPriLength;

  uint8_t m_sharedSecret[SIGN_ON_BASIC_CLIENT_SHARED_SECRET_MAX_LENGTH];
  size_t m_sharedSecretLength;

  SignOnBasicClientTimer m_timer;

  // ** //

  ndn_TlvDecoder m_received_certificate_decoder;

  // ** //

};

#endif // SIGN_ON_BASIC_CLIENT_HPP
