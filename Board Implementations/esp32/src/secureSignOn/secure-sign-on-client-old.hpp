// #ifndef ONBOARDING_CLIENT_BASIC_HPP
// #define ONBOARDING_CLIENT_BASIC_HPP

// #include <esp8266ndn.h>
// #include "../consts.hpp"
// #include "../helpers/security-helper.hpp"
// #include <PriUint64.h>

// /** \brief Onboarding client for secure sign-on protocol.
//  */
// class OnboardingClientBasic : public ndn::PacketHandler
// {
// public:
//   explicit
//   OnboardingClientBasic(ndn::Face& face, const uint8_t* BKpub, const uint8_t* BKpri);

//   ~OnboardingClientBasic() noexcept;

//   bool
//   begin();

//   bool
//   initiateSignOn();

//   bool
//   onboardingCompleted() {
//     return m_onboarding_completed;
//   }

//   const ndn::NameLite&
//   getDeviceID() {
//     return m_deviceID;
//   }

//   const ndn::NameLite&
//   getNetworkPrefix() {
//     return m_networkPrefix;
//   }

// private:

//   bool
//   sendBootstrappingRequest();

//   bool 
//   sendCertificateRequest();

//   bool 
//   processData(const ndn::DataLite& data, uint64_t endpointId) override;

//   bool
//   processBootstrappingResponse(const ndn::DataLite& data, uint64_t endpointId);

//   bool
//   processCertificateRequestResponse(const ndn::DataLite& data, uint64_t endpointId);

//   bool
//   AKpubRetrieved() {
//     for (int i = 0; i < 64; i++) {
//       if (m_AKpub[i] != 0)
//         return true;
//     }
//     return false;
//   }

// private:
//   ndn::Face& m_face;

//   char m_bootstrapPrefixString[13] = "/ndn/sign-on";
//   ndn_NameComponent m_bootstrapPrefixComps[2];
//   ndn::NameLite m_bootstrapPrefix;

//   char m_certNameComponentString[6] = "/cert";
//   ndn_NameComponent m_certNameComponentComps[1];
//   ndn::NameLite m_certNameComponent;

//   const uint8_t* m_BKpub;
//   const uint8_t* m_BKpri;

//   ndn_NameComponent m_dummyKeyComp[1];
//   ndn::NameLite m_dummyKeyName;

//   ndn_NameComponent m_deviceIDNameComp[1];
//   ndn::NameLite m_deviceID;

//   ndn_NameComponent m_networkPrefixComps[MAX_NAME_COMPS];
//   ndn::NameLite m_networkPrefix;

//   std::unique_ptr<ndn::PacketBuffer> m_lastBootstrapResponse;

//   size_t m_N1pub_length;
//   uint8_t m_N1pub[500];
//   size_t m_N1pri_length;
//   uint8_t m_N1pri[500];

//   size_t m_N2pub_length;
//   uint8_t m_N2pub[500];

//   uint8_t m_N1pubXORN2pub[64];

//   size_t m_Dkpub_length;
//   uint8_t m_DkPub[500];
//   size_t m_Dkpri_length;
//   uint8_t m_DkPri[500];

//   uint8_t m_ECC_PUBLIC_DIGEST[ndn_SHA256_DIGEST_SIZE];
//   uint8_t m_BKpubMacByDKpub[ndn_SHA256_DIGEST_SIZE];
//   char m_BKpubDigestChars[65];

//   ndn::EcPrivateKey m_bootstrapPrivateKey;
//   size_t m_sharedSecret_length;
//   uint8_t m_sharedSecret[500];

//   SecurityHelper m_securityHelper;

//   size_t m_AKpub_length;
//   uint8_t m_AKpub[500];
//   uint8_t m_trustAnchor[3000];
//   size_t m_trustAnchorLength;

//   uint8_t m_trustAnchorDigest[ndn_SHA256_DIGEST_SIZE];

//   bool m_onboarding_completed;

// };

// #endif // ONBOARDING_CLIENT_BASIC_HPP
